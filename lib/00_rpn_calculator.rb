class RPNCalculator


  def initialize()
    @list = []
  end

  def push(int)
    @list << int
  end

  def value()
    @list.last
  end

  def plus()
    raise "calculator is empty" if @list.length < 2
    operation(:+)
  end

  def minus()
    raise "calculator is empty" if @list.length < 2
    operation(:-)
  end

  def divide()
    raise "calculator is empty" if @list.length < 2
    operation(:/)
  end

  def times()
    raise "calculator is empty" if @list.length < 2
    operation(:*)
  end

  def tokens(string)
    string.split(" ").map do |el|
      operation?(el) ? el.to_sym : Integer(el)
    end
  end

  def evaluate(string)
    rpn = tokens(string)
    rpn.each do |tok|
      case tok
      when Integer
        push(tok)
      else
        operation(tok)
      end
    end
    value

  end




  private

  def operation?(char)
    [:+, :*, :/, :-].include?(char.to_sym)
  end

  def operation(op_symbol)
    first = @list.pop
    second = @list.pop
    case op_symbol
    when :+
      @list.push(first + second)
    when :-
      @list.push(second - first)
    when :*
      @list.push(first * second)
    when :/
      @list.push(second.to_f / first.to_f)
    else
      @list.push(first)
      @list.push(second)
      raise "No such operation given: #{op_symbol}"

    end

  end




end
